import { Avatar, Box, Fab, Grid, Typography } from "@mui/material";
import { AcUnitRounded, AddRounded } from "@mui/icons-material";
// import AcUnitIcon from '@mui/icons-material/AcUnit';
export default function HomeHeader() {
  return (
    <Box>
      <Grid container spacing={2}>
        <Grid item xs={1}>
          <AcUnitRounded fontSize="large" />
        </Grid>
        <Grid item xs={10}>
          <Typography variant="h4" sx={{ fontWeight: "bolder", textAlign: 'center' }}>
            SMART HOME
          </Typography>
        </Grid>
        <Grid item xs={1}>
          <Avatar sx={{ backgroundColor: "#42a5f5" }}>KN</Avatar>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={10}>
          <Typography
            variant="h5"
            sx={{ fontWeight: "bold", marginTop: "15px" }}
          >
            Welcome
          </Typography>
          <Typography sx={{ fontSize: "1.1rem" }}>
            Good morning Mlle. Keshia
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <Fab color="secondary" aria-label="add">
            <AddRounded />
          </Fab>
        </Grid>
      </Grid>
    </Box>
  );
}
