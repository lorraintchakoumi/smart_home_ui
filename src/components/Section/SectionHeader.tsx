import {
  Avatar,
  Box,
  Grid,
  IconButton,
  Tooltip,
  Typography,
} from "@mui/material";
import { ArrowBackRounded } from "@mui/icons-material";
import { useNavigate } from "react-router";

export default function SectionHeader({
  title,
}: {
  title: string | undefined;
}) {
  const navigate = useNavigate();
  return (
    <Box sx={{ marginBottom: "20px" }}>
      <Grid container spacing={2}>
        <Grid item xs={1}>
          <Tooltip arrow title="Back to home">
            <IconButton
              size="small"
              color="primary"
              onClick={() => navigate("/")}
            >
              <ArrowBackRounded fontSize="large" />
            </IconButton>
          </Tooltip>
        </Grid>
        <Grid item xs={10}>
          <Typography variant="h4" sx={{ fontWeight: "bolder" }}>
            {title ?? "N/A"}
          </Typography>
        </Grid>
        <Grid item xs={1}>
          <Avatar sx={{ backgroundColor: "#42a5f5" }}>KN</Avatar>
        </Grid>
      </Grid>
      <Typography variant="h5" sx={{ fontWeight: "bold", marginTop: "15px" }}>
        Welcome
      </Typography>
      <Typography sx={{ fontSize: "1.1rem" }}>
        Good morning Mlle. Keshia
      </Typography>
    </Box>
  );
}
