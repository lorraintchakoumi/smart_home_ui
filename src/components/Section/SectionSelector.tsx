import { Chip } from "@mui/material";
import { CardData } from "../../routes";
import {useParams} from 'react-router'

export default function SectionSelector({
  chip: { isActive, cardId, title },
}: {
  chip: CardData;
}) {
  const activateChip = (section_id: number) => {
    console.log(`New Active: ${section_id}`);
  };
  const {section_id} = useParams()
  return (
    <Chip
      color="primary"
      sx={{margin: '0 2px'}}
      label={title}
      variant={Number(section_id) === cardId ? "filled" : "outlined"}
      onClick={() => activateChip(cardId)}
    />
  );
}
