import HomeHeader from "../components/HomeHeader";
import HomeCard from "../components/HomeCard";
import { Container, Grid } from "@mui/material";
import { CardData } from "../routes";

export default function HomePage({ homeCards }: { homeCards: CardData[] }) {
  return (
    <Container
      sx={{ padding: "10px 0", backgroundColor: "#d3dbeb", height: "100vh" }}
    >
      <HomeHeader />
      <Grid container spacing={2} sx={{ marginTop: "7px" }}>
        {homeCards.map((homeCard, index) => (
          <Grid item xs={3}>
            <HomeCard cardData={homeCard} key={index} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
