import { ArrowForwardIosRounded } from "@mui/icons-material";
import { Grid, Paper, Switch, Typography } from "@mui/material";
import { Equipment } from "../../pages/SectionPage";

export default function EquipmentCard({
  equip: { icon, title, brand, isActive, hasMore },
}: {
  equip: Equipment;
}) {
  return (
    <Paper elevation={1} sx={{margin: '20px 0'}}>
      <Grid container spacing={1}>
        <Grid item xs={1} sx={{textAlign: 'center', alignSelf:'center'}}>
          {icon}
        </Grid>
        <Grid container item xs={10}>
          <Grid item xs={12}>
            <Typography variant="h5">{title}</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" sx={{ color: "rgba(0,0,0,0.5)", fontSize: '0.9rem' }}>
              {brand}
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={1} sx={{alignSelf: "center", textAlign: "center"}}>
          {hasMore ? (
            <ArrowForwardIosRounded />
          ) : (
            <Switch checked={isActive} size="medium" />
          )}
        </Grid>
      </Grid>
    </Paper>
  );
}
