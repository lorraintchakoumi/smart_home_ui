import { Container } from "@mui/system";
import SectionHeader from "../components/Section/SectionHeader";
import SectionSelector from "../components/Section/SectionSelector";
import { CardData } from "../routes";
import {
  WeekendRounded,
  KitchenRounded,
  ShowerRounded,
  YardRounded,
  DeckRounded,
  StoreRounded,
  DesktopWindowsRounded,
  FlourescentRounded,
  VolumeMuteRounded,
  WbIncandescentRounded,
  InvertColorsRounded,
} from "@mui/icons-material";
import EquipmentCard from "../components/Section/EquipmentCard";
import { Navigate, useParams } from "react-router";

export interface Equipment {
  icon: JSX.Element;
  title: string;
  brand: string;
  isActive: boolean;
  hasMore: boolean;
}

const equipments: Equipment[] = [
  {
    icon: <WeekendRounded sx={{ fontSize: 55, color: "rgba(0,0,0,0.65)" }} />,
    title: "Air conditioner",
    brand: "LG",
    isActive: false,
    hasMore: true,
  },
  {
    icon: (
      <InvertColorsRounded sx={{ fontSize: 55, color: "rgba(0,0,0,0.65)" }} />
    ),
    title: "Water Geyser",
    brand: "TATA",
    isActive: true,
    hasMore: false,
  },
  {
    icon: (
      <WbIncandescentRounded sx={{ fontSize: 55, color: "rgba(0,0,0,0.65)" }} />
    ),
    title: "Light",
    brand: "Syska led lights",
    isActive: true,
    hasMore: false,
  },
  {
    icon: (
      <VolumeMuteRounded sx={{ fontSize: 55, color: "rgba(0,0,0,0.65)" }} />
    ),
    title: "Home Theatre",
    brand: "Bose",
    isActive: false,
    hasMore: false,
  },
  {
    icon: (
      <FlourescentRounded sx={{ fontSize: 55, color: "rgba(0,0,0,0.65)" }} />
    ),
    title: "Night Lamp",
    brand: "Syska",
    isActive: false,
    hasMore: false,
  },
  {
    icon: (
      <DesktopWindowsRounded sx={{ fontSize: 55, color: "rgba(0,0,0,0.65)" }} />
    ),
    title: "Television",
    brand: "Samsung",
    isActive: true,
    hasMore: false,
  },
];

export default function SectionPage({ homeCards }: { homeCards: CardData[] }) {
  const { section_id } = useParams();
  return (
    <Container
      sx={{ padding: "10px 0", backgroundColor: "#d3dbeb", height: "100vh" }}
    >
      <SectionHeader
        title={
          homeCards.find((card) => card.cardId === Number(section_id))?.title
        }
      />
      {homeCards.map((card, index) => (
        <SectionSelector chip={card} key={index} />
      ))}
      {homeCards.find((card) => card.cardId === Number(section_id)) ? (
        equipments.map((equip, index) => (
          <EquipmentCard equip={equip} key={index} />
        ))
      ) : (
        <Navigate to="/" />
      )}
    </Container>
  );
}
