import { Navigate } from "react-router";
import HomePage from "./pages/HomePage";
import SectionPage from "./pages/SectionPage";
import {
  WeekendRounded,
  KitchenRounded,
  ShowerRounded,
  YardRounded,
  DeckRounded,
  StoreRounded,
} from "@mui/icons-material";

export interface CardData {
  backgroundColor: string;
  title: string;
  color: string;
  icon: JSX.Element;
  cardId: number;
  isActive: boolean;
}

const homeCards: CardData[] = [
  {
    cardId: 1,
    backgroundColor: "rgba(0,0,0,0.7)",
    icon: <WeekendRounded fontSize="large" />,
    title: "Living Room",
    color: "#1976d2",
    isActive: false,
  },
  {
    cardId: 2,
    backgroundColor: "rgba(0,0,0,0.7)",
    icon: <KitchenRounded fontSize="large" />,
    title: "Kitchen",
    color: "#1976d2",
    isActive: true,
  },
  {
    cardId: 3,
    backgroundColor: "rgba(0,0,0,0.7)",
    icon: <StoreRounded fontSize="large" />,
    title: "Hall",
    color: "#1976d2",
    isActive: false,
  },
  {
    cardId: 4,
    backgroundColor: "rgba(0,0,0,0.7)",
    icon: <ShowerRounded fontSize="large" />,
    title: "Bathroom",
    color: "#1976d2",
    isActive: false,
  },
  {
    cardId: 5,
    backgroundColor: "rgba(0,0,0,0.7)",
    icon: <DeckRounded fontSize="large" />,
    title: "Balcony",
    color: "#1976d2",
    isActive: false,
  },
  {
    cardId: 6,
    backgroundColor: "rgba(0,0,0,0.7)",
    icon: <YardRounded fontSize="large" />,
    title: "Garden",
    color: "#1976d2",
    isActive: false,
  },
];

export const routes = [
  { path: "/", element: <HomePage homeCards={homeCards} /> },
  { path: "/section/:section_id", element: <SectionPage homeCards={homeCards}/> },
  { path: "*", element: <Navigate to="/" /> },
];
