import { Box, Paper, Typography } from "@mui/material";
import { CardData } from "../routes";
import { useNavigate } from "react-router";

export default function HomeCard({
  cardData: { backgroundColor, icon, title, color, cardId },
}: {
  cardData: CardData;
}) {
  const navigate=useNavigate()
  return (
    <Paper
    onClick={()=>navigate(`section/${cardId}`)}
      elevation={3}
      sx={{
        "&:hover": {
          backgroundColor: backgroundColor,
          transition: ".5s",
        },
        display: "grid",
        transition: ".5s",
        justifyItems: "center",
        padding: "20px",
        position: "relative",
      }}
    >
      <Typography
        sx={{
          "&::after": {
            height: "10px",
            width: "10px",
            content: '""',
            backgroundColor: color,
            position: "absolute",
            top: '10px',
            right: '10px',
            borderRadius: "100%",
          },
        }}
      />
      <Box
        sx={{
          borderRadius: "100%",
          backgroundColor: color,
          padding: "10px",
          textAlign: "center",
        }}
      >
        {icon}
      </Box>
      <Typography
        variant="h6"
        sx={{ fontWeight: "bold",  marginTop: "7px" }}
      >
        {title}
      </Typography>
    </Paper>
  );
}
