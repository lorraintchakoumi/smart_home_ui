import {routes} from "./routes";
import { useRoutes } from "react-router";

function App() {
  const routing = useRoutes(routes);
  return routing;
}

export default App;
